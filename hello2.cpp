///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World II
//
// Result:
//   Says hello in c++ without using namespace std
//
//
// @author Geoffrey Teocson <gteocson@hawaii.edu>
// @date   15/02/2021
///////////////////////////////////////////////////////////////////////////////



#include <iostream>

int main() {


   std::cout << "Hello World from hello2.cpp" << std::endl;

   return 1;
}
