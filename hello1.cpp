///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World I
//
// Result:
//   Says hello in c++ using namespace std
//   
//
// @author Geoffrey Teocson <gteocson@hawaii.edu>
// @date   15/02/2021
///////////////////////////////////////////////////////////////////////////////


#include<iostream>

using namespace std;

int main() {

cout << "Hello World from hello1.cpp" << endl;

return 0;


}
